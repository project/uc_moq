
DESCRIPTION
===========
This module allows Ubercart store administrators to set the minimum and maximum
number of items to be in the shopping cart before a user is allowed to check
out.

INSTALLATION
============
Installation is like any other module. Uncompress the tar-ball file, .tar.gz,
and drop it into the "sites/all/modules" subdirectory.
Visit Administer >> Site building >> Modules, tick the box in front of the
module name and press "Save configuration".

CONFIGURATION
=============
Enter optional shopping cart minimum and maximum order quantities at 
Store administration >> Configuration >> Cart settings >> Edit
(admin/store/settings/cart/edit/basic) 
Press "Save configuration".

USAGE
=====
An error message will appear whenever a user tries to check out with 
insufficient or too many items in their cart.

UNINSTALL
=========
Disable as per normal at the Administer >> Site building >> Modules page. Then
click the Uninstall tab.
